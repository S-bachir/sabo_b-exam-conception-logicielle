from fastapi import FastAPI
import requests
from CardNumber import CardNumber


APP = FastAPI()
id = ""

@APP.get("/creer-un-deck")
def createdeck():
    
    #modify the global variable "id" within the function
    global id

    request = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    request_js = request.json()

    id = request_js["deck_id"]
    return {"deck_id":id}

 

@APP.post("/cartes")
def drawcards(card:CardNumber):
    global id

	
    #if there is no deck already created, we can create another one before drawing
    if (id == ""):
        createdeck()


    request = requests.get("https://deckofcardsapi.com/api/deck/"+str(id)+"/draw/?count="+str(card.card_number))
    request_js = request.json()
    
    
    return ({"deck_id":  id, "cards": request_js["cards"]})
