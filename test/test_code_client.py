from unittest import TestCase, main

from client.code_client import Count


class TestCount(TestCase):
    def test_count(self):


        #GIVEN

        deck_1 = [{'code': 'AD', 'image': 'https://deckofcardsapi.com/static/img/aceDiamonds.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/aceDiamonds.svg', 'png': 'https://deckofcardsapi.com/static/img/aceDiamonds.png'}, 'value': 'ACE', 'suit': 'DIAMONDS'}, 
        {'code': '2D', 'image': 'https://deckofcardsapi.com/static/img/2D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/2D.svg', 'png': 'https://deckofcardsapi.com/static/img/2D.png'}, 'value': '2', 'suit': 'DIAMONDS'}, 
        {'code': '7C', 'image': 'https://deckofcardsapi.com/static/img/7C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/7C.svg', 'png': 'https://deckofcardsapi.com/static/img/7C.png'}, 'value': '7', 'suit': 'CLUBS'}, 
        {'code': 'JC', 'image': 'https://deckofcardsapi.com/static/img/JC.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/JC.svg', 'png': 'https://deckofcardsapi.com/static/img/JC.png'}, 'value': 'JACK', 'suit': 'CLUBS'}, 
        {'code': '2S', 'image': 'https://deckofcardsapi.com/static/img/2S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/2S.svg', 'png': 'https://deckofcardsapi.com/static/img/2S.png'}, 'value': '2', 'suit': 'SPADES'}, 
        {'code': 'KD', 'image': 'https://deckofcardsapi.com/static/img/KD.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KD.svg', 'png': 'https://deckofcardsapi.com/static/img/KD.png'}, 'value': 'KING', 'suit': 'DIAMONDS'}, 
        {'code': 'QH', 'image': 'https://deckofcardsapi.com/static/img/QH.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/QH.svg', 'png': 'https://deckofcardsapi.com/static/img/QH.png'}, 'value': 'QUEEN', 'suit': 'HEARTS'}, 
        {'code': '9H', 'image': 'https://deckofcardsapi.com/static/img/9H.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/9H.svg', 'png': 'https://deckofcardsapi.com/static/img/9H.png'}, 'value': '9', 'suit': 'HEARTS'}, 
        {'code': 'KS', 'image': 'https://deckofcardsapi.com/static/img/KS.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KS.svg', 'png': 'https://deckofcardsapi.com/static/img/KS.png'}, 'value': 'KING', 'suit': 'SPADES'}, 
        {'code': '0D', 'image': 'https://deckofcardsapi.com/static/img/0D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/0D.svg', 'png': 'https://deckofcardsapi.com/static/img/0D.png'}, 'value': '10', 'suit': 'DIAMONDS'}]

        #WHEN
        count_1 = Count(deck_1)

        #THEN 
        self.assertEqual({'HEARTS': 2, 'SPADES': 2, 'DIAMONDS': 4, 'CLUBS': 2},count_1)


if __name__ == '__main__':
     main()
