from unittest import TestCase, main
from client.code_client import count

class TestCount(TestCase):
    def test_count(self, deck, detail_count):


        #GIVEN

        deck_1 = deck
        #WHEN
        count_1 = count(deck_1)

        #THEN 
        self.assertEqual(detail_count,count_1)

if __name__ == '__main__':
     main()