

import requests
import time

local_host = "http://127.0.0.1:8000/"

#deck identification selection 
request = requests.get(local_host+"creer-un-deck")
id = request.json()["deck_id"]
print("deck id is = " + id)




 #number of card selection
print("how many cards do you you want?")
nb_cards = input("--> ")

#verify is the input is a number. If not, retry. 
try:
    nb_cards = int(nb_cards)
except:
    print("please enter a valid number of card ")
    nb_cards = input("--> ")
    try :
        nb_cards = int(nb_cards)
    except :
        nb_cards = 10  #define a number of  number if the input isn't a number aftter 2 attempts (this applciation is very impatient)

print("the number of card selected is : {}".format(nb_cards))
time.sleep(2)  




# cards drawing 
card_number_json = {"card_number": nb_cards}
request = requests.post(local_host+"cartes", json=card_number_json)
response = request.json()

deck_cards = response["cards"]

print(deck_cards)

#detailed count of different card colors 
def Count(deck_cards):
    detailed_count = {"HEARTS": 0, "SPADES": 0, "DIAMONDS": 0, "CLUBS": 0}

    for card in deck_cards:
        detailed_count[card["suit"]] += 1

    return detailed_count


print("""\nDifferent cards selected are : 
                   
                        
             {}
             

             """.format(Count(deck_cards)))

